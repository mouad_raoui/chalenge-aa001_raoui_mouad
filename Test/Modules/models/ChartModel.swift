//
//  ChartModel.swift
//  Test
//
//  Created by raoui mouad on 9/25/20.
//

import Foundation



struct ResponseChart: Codable {
    let rates: [String: Rates]
    let startAt, base, endAt: String

    enum CodingKeys: String, CodingKey {
        case rates
        case startAt = "start_at"
        case base
        case endAt = "end_at"
    }
}

// MARK: - Rate
struct Rates : Codable {
    let symbol: Double


    private struct DynamicCodingKeys: CodingKey {

        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        var intValue: Int?
        init?(intValue: Int) {
           
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: DynamicCodingKeys.self)
        let decodedObject = try container.decode(Double.self, forKey: DynamicCodingKeys(stringValue: container.allKeys.first!.stringValue)!)
        self.symbol = decodedObject

    }
    
}



