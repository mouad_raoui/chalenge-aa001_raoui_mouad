//
//  HomeModel.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import Foundation




struct ResponseCurrency : Codable {
    let rates: [String: Double]
    let base, date: String
}
