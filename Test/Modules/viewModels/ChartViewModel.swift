//
//  ChartViewModel.swift
//  Test
//
//  Created by raoui mouad on 9/25/20.
//

import Foundation
import Charts

class ChartViewModel  {
    //Marks : Variables
    let networkManage : NetworkManager
    var dates : [Date] = []  
    var values : [Double] = []
    //Marks : Completions
    var chartDidUpdate: (() -> (Void))?
    
    
    init(networkManage : NetworkManager) {
        self.networkManage = networkManage
    }
    
    
    func getCurrencyTwoMonthExchange(base: String , symbols: String , completion : @escaping (Bool) -> () )  {
        self.networkManage.getCurrencyChart(base: base , symbols: symbols).done { [self] (data) in
            
            
            let array = data.rates.sorted(by : {$0.key.convertStringTodate().compare($1.key.convertStringTodate()) == .orderedAscending})
            self.dates = array.map{$0.0.convertStringTodate()}
            self.values = array.map{Double(String(format: "%.3f", $0.1.symbol))!}
            
            completion(false)
            
        }.catch { (error) in
            completion(true)
        }
    }
    
    
    
    func getDateDoube(index : Int) -> Double {
        let date = self.dates[index].timeIntervalSince1970
        return Double(date)
    }
    func numberOfValue() -> Int {
        return self.dates.count
    }
    
    func updateGraph() -> LineChartData {
        let data = LineChartData()
        var lineChartEntry  = [ChartDataEntry]()
        for i in 0..<self.numberOfValue() {
            let value = ChartDataEntry(x: self.getDateDoube(index: i) , y: self.values[i])
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Dates")
        line1.colors = [NSUIColor.blue]
        data.addDataSet(line1)
        return data
    }
    
}
