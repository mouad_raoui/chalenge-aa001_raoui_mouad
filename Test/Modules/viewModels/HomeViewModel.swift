//
//  HomeViewModel.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import Foundation


class HomeViewModel  {
    let networkManage : NetworkManager
    
    var keyCurrency : [String ] = [] {
        didSet {
            self.currencyDidChanges!(self.error)
        }
    }
    var valueCurrency : [Double] = []
    var base : String = ""
    var currencyDidChanges: ((Bool) -> (Void))?
    var error : Bool = false
    
    
    
    init(networkManage : NetworkManager) {
        self.networkManage = networkManage
    }
    
    
    
    func getCurrency(base : String )  {
        self.networkManage.getCurrency(base: base).done { (data) in
            
            self.error = false
            self.base = data.base
            self.keyCurrency = data.rates.map {$0.0}
            self.valueCurrency = data.rates.map {$0.1}
            
        }.catch { (error) in
            self.error = true 
        }
    }
    
    func numberOfRow() -> Int {
        return self.keyCurrency.count
    }
    
    func getRateKey(index : Int) -> String {
        return self.keyCurrency[index]
    }
    
    func getRateValue(index : Int) -> String {
        return "\(self.valueCurrency[index])"
    }
    
    func getBase() -> String {
        return self.base
    }
    
    
    
}



