//
//  ChartViewController.swift
//  Test
//
//  Created by raoui mouad on 9/25/20.
//

import UIKit
import Charts
class ChartViewController: BaseViewController {
    
    
    
    // MARK:  OUTLETS
    @IBOutlet weak var chartView: LineChartView!
    
    // MARK:  Variables
    var chartViewModel : ChartViewModel?
    var base   = ""
    var symbols = ""
    
    // MARK:  Constants
    
    let ROTATION_LABEL_CHART : CGFloat = 90
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initChart()
        self.chartViewModel = ChartViewModel(networkManage: NetworkManager())
        self.activityIndicator()
        self.chartViewModel?.getCurrencyTwoMonthExchange(base: base, symbols: symbols, completion: {error  in
            self.removeActivityIndicator()
            if !error {
                self.chartView.data = self.chartViewModel?.updateGraph()
            }else{
                self.showAlert(message: StringsConstants.ErrorMessageWs)
            }
            
        })
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func initChart()  {
        chartView.xAxis.valueFormatter = self
        chartView.chartDescription?.text = self.base + " -> " + self.symbols
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelRotationAngle = ROTATION_LABEL_CHART
    }
    
}


// MARK: axisFormatDelegate
extension ChartViewController: IAxisValueFormatter,ChartViewDelegate {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let date =  Date(timeIntervalSince1970: value)
        return date.formateDate()
    }
    
    
}
