//
//  HomeViewController.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import UIKit

class HomeViewController: BaseViewController {
    
    // MARK:  Marks
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var currencyTextField: UITextField!
    
    
    
    // MARK:  Variables
    
    var homeViewModel : HomeViewModel!
    let networkManager = NetworkManager()
    var currencySelected : Int = 0
    let picker = UIPickerView()
    
    // MARK:  Constants
    
    let HEIGHT_CELL  : CGFloat = 80
    let CELL_XIB_NAME  : String = "HomeTableViewCell"
    let CONTROLLER_XIB_NAME  : String = "ChartViewController"
    let NAVIGATION_TITLE = "Currency"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = NAVIGATION_TITLE
        self.homeViewModel = HomeViewModel(networkManage: self.networkManager)
        
        self.homeViewModel.getCurrency(base: self.currencyTextField.text ?? "" )
        
        self.activityIndicator()
        self.homeViewModel.currencyDidChanges = {
            
            error in
            self.removeActivityIndicator()
            if !error {
                self.setUpPicker(picker: self.picker, textField: self.currencyTextField)
                self.currencyTextField.text = self.homeViewModel.getBase()
                self.currencyTextField.tintColor = .clear
                self.tableView.reloadData()
            }else{
                self.showAlert(message: StringsConstants.ErrorMessageWs)
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUpPicker(picker : UIPickerView , textField : UITextField )  {
        
        picker.delegate = self
        picker.dataSource = self
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: StringsConstants.Done, style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringsConstants.Cancel, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        textField.inputView = picker
    }
    
    @objc func doneDatePicker(sender:UIButton ){
        
        self.currencyTextField.text = self.homeViewModel.getRateKey(index: self.currencySelected)
        self.homeViewModel.getCurrency(base: self.currencyTextField.text ?? "" )
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    
}


extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeViewModel.numberOfRow()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_XIB_NAME, for: indexPath) as! HomeTableViewCell
        
        cell.setUpCell(symbol: self.homeViewModel.getRateKey(index: indexPath.row), value: self.homeViewModel.getRateValue(index: indexPath.row))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HEIGHT_CELL
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chartViewController = UIStoryboard.mainStoryboard.instantiateViewController(withIdentifier: CONTROLLER_XIB_NAME ) as! ChartViewController
        chartViewController.base = self.currencyTextField.text ?? ""
        chartViewController.symbols = self.homeViewModel.getRateKey(index: indexPath.row)
        self.navigationController?.pushViewController(chartViewController, animated: true)
    }
    
}
extension HomeViewController :  UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.homeViewModel.numberOfRow()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.homeViewModel.getRateKey(index: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currencySelected = row
        
        
    }
    
    
}
