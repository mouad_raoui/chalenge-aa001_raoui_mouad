//
//  HomeTableViewCell.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    //Marks : OUTLETS
    @IBOutlet weak var currencyValueLbl: UILabel!
    @IBOutlet weak var currencyNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpCell(symbol : String , value : String)  {
        self.currencyValueLbl.text = value
        self.currencyNameLbl.text = symbol
    }
}
