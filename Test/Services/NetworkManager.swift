//
//  NetworkManager.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import Foundation

import PromiseKit
import Alamofire
struct NetworkManager {
    
    
    
    
    func getCurrency(base : String ) -> Promise<ResponseCurrency> {
        let url = APPURL.Latest
        let parameters : Parameters = ["base":base]
        return AF.request(url,parameters: parameters).responseCodable()
    }
    
    func getCurrencyChart(base : String , symbols : String ) -> Promise<ResponseChart> {
        let url = APPURL.History
        let now = Date()
        let previousMonth = Calendar.current.date(byAdding: .month, value: -2, to: Date())
        
        let parameters : Parameters = ["base":base,"symbols":symbols,"end_at":now.formateDate(),"start_at":previousMonth!.formateDate()]
        
        return AF.request(url,parameters: parameters).responseCodable()
    }
}
