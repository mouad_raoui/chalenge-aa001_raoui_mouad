//
//  BaseViewController.swift
//  Test
//
//  Created by raoui mouad on 9/27/20.
//

import UIKit


class  BaseViewController : UIViewController {
    var activityIndicator = UIActivityIndicatorView()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    var strLabel = UILabel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func activityIndicator(_ title: String = StringsConstants.Loading, _ color : UIColor = .black ,_ width : CGFloat = SCREEN_WIDTH, _ height : CGFloat = SCREEN_HEIGHT ) {

        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()

        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        strLabel.textColor = UIColor.white

        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true

        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.color = color
        activityIndicator.startAnimating()

        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        
        effectView.center = CGPoint(x: width  / 2, y: height / 2)
        view.addSubview(effectView)
        view.isUserInteractionEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func removeActivityIndicator(){
        DispatchQueue.main.async {
            self.effectView.removeFromSuperview()
            self.view.isUserInteractionEnabled = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func popupAlert(title: String?, message: String?,style : UIAlertController.Style = UIAlertController.Style.alert, actionTitles:[(title : String?,style : UIAlertAction.Style)], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        for (index, action) in actionTitles.enumerated() {
            let action = UIAlertAction(title: action.title, style: action.style, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(message : String){
        self.popupAlert(title: nil, message: message, style: .alert, actionTitles: [("Close",.default)], actions: [nil])
    }
}
