//
//  Constants.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import UIKit

let SCREEN_WIDTH                    =   UIScreen.main.bounds.size.width
let SCREEN_HEIGHT                   =   UIScreen.main.bounds.size.height
struct StringsConstants {
    static var ErrorMessageWs    = "Please Try Again"
    static var InternetError    = "Please check your network"
  
    static var Done    = "Done"
    static var Cancel    = "Cancel"
    static var Loading    = "Loading"
}
