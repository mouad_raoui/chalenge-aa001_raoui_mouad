//
//  URLConstants.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import Foundation




struct APPURL {
    
    private struct Domains {
        static let DEV = "https://api.exchangeratesapi.io/"
       
        
    }
    
    
    private  static let BaseURL = Domains.DEV
  
 
    static var History :  String {
        return BaseURL  + "history"
    }
    
    static var Latest :  String {
        return BaseURL  + "latest"
    }
    

    
   
    
}



