//
//  AppDelegate.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import Alamofire
import PromiseKit
import SystemConfiguration
extension Alamofire.DataRequest {
    
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("debuglog: ",self)
        print(self)
        #endif
        return self
    }
    
    // Return a Promise for a Codable
    public func responseCodable<T: Decodable>() -> Promise<T> {
        return Promise { seal in
            responseData(queue: DispatchQueue.main) { response in
                if Reachability.isConnectedToNetwork(){
                    #if DEBUG
                    debugPrint("response.result: ",response.result)
                    #endif
                    switch response.result {
                    case .success(let value):
                        if let httpStatusCode = response.response?.statusCode {
                           print("httpStatusCodeSuccess:",httpStatusCode)
                           
                        }
                        if let httpResponsedata = response.response {
                            #if DEBUG
                            print("httpResponsedata:",httpResponsedata)
                            #endif
                        }
                        debugPrint(value)
                        
                        let decoder = JSONDecoder()
                        do {
                            seal.fulfill(try decoder.decode(T.self, from: value))
                        } catch let e {
                            #if DEBUG
                            print("Error.parse: ",e.localizedDescription)
                            #endif
                            seal.reject(e)
                        }
                    case .failure(let error):
                        var e = error as NSError
                        if let httpStatusCode = response.response?.statusCode {
                            #if DEBUG
                            print("httpStatusCodeError:",httpStatusCode)
                            #endif
                            
                            e = NSError(domain: StringsConstants.ErrorMessageWs, code: httpStatusCode, userInfo:nil)
                            seal.reject(e)
                        }
                        seal.reject(e)
                    }
                }
                else{
                    let e = NSError(domain: StringsConstants.InternetError, code: -12, userInfo: nil)
                    seal.reject(e)
                }
            }
        }
    }
}

open class Reachability
{
    class func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress) }
        }

        var flags = SCNetworkReachabilityFlags()

        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) { return false }

        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
