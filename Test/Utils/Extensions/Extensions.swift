//
//  UIView + Extensions.swift
//  Test
//
//  Created by raoui mouad on 9/24/20.
//

import UIKit


extension UIView  {
    
    @IBInspectable var cornerRadius: CGFloat { get { return layer.cornerRadius } set { layer.cornerRadius = newValue; layer.masksToBounds = newValue > 0 } }
    
    @IBInspectable var borderWidth: CGFloat { get { return layer.borderWidth } set { layer.borderWidth = newValue } }
    
    @IBInspectable var borderColor: UIColor? { get { return UIColor(cgColor: layer.borderColor!) } set { layer.borderColor = newValue?.cgColor } }
}


extension UIStoryboard {
    
    static var mainStoryboard: UIStoryboard {
           return UIStoryboard(name: "Main", bundle: Bundle.main)
       }
}

extension Date {
    
    func formateDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
}


extension String  {
    
    func convertStringTodate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: self) ?? Date()
    }
    
}
